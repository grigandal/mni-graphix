import React, { isValidElement } from "react";
import {
    Container,
    Header,
    Content,
    Footer,
    Sidebar,
    Navbar,
    Nav,
    Dropdown,
    Icon,
    Modal,
    Button,
    Input,
    SelectPicker,
    Popover,
    Whisper,
    List,
} from "rsuite";
import SuperGraph from "./SuperGraph";

function download(filename, text) {
    var element = document.createElement("a");
    element.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(text));
    element.setAttribute("download", filename);

    element.style.display = "none";
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

function upload(callback) {
    var input = document.createElement("input");
    input.type = "file";
    input.oninput = (e) => {
        var file = e.target.files[0];
        var reader = new FileReader();
        reader.onload = () => {
            if (typeof callback == typeof (() => {})) {
                callback(reader.result);
            }
        };
        reader.readAsText(file);
    };
    input.click();
}

class Prompt extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: props.request.map((e) => {
                return Object.assign(e, {
                    value: e.initialValue !== undefined ? e.initialValue : this.getEmptyValue(e.type),
                });
            }),
        };
    }

    getEmptyValue = (type) => {
        switch (type) {
            case "input":
                return "";
            case "select":
                return undefined;
        }
    };

    getBody = (request) => {
        return request.map((e, i) => {
            switch (e.type) {
                case "input":
                    var props = {
                        value: this.state.data[i] ? this.state.data[i].value : "",
                        onChange: (v) => {
                            this.setState((state) => {
                                state.data[i].value = v;
                                return state;
                            });
                        },
                    };
                    props = Object.assign(props, e.placeholder ? { placeholder: e.placeholder } : {});

                    return (
                        <div style={{ marginTop: 10 }}>
                            <Input {...props}></Input>
                        </div>
                    );
                case "label":
                    return (
                        <div style={{ marginTop: 10 }}>
                            <label>{e.text}</label>
                        </div>
                    );

                case "select":
                    var props = {
                        data: e.data,
                        onChange: (s) => {
                            this.setState((state) => {
                                state.data[i].value = s;
                                return state;
                            });
                        },
                    };
                    props = Object.assign(
                        props,
                        this.state.data[i] && this.state.data[i].value ? { value: this.state.data[i].value } : {},
                        e.placeholder ? { placeholder: e.placeholder } : {}
                    );
                    return (
                        <div style={{ marginTop: 10 }}>
                            <SelectPicker {...props}></SelectPicker>
                        </div>
                    );
            }
        });
    };

    getData = () => {
        return this.state.data.map((e) => e);
    };

    render() {
        if (this.state.data.length != this.props.request.length) {
            this.setState((state) => {
                state.data = this.props.request.map((e) => {
                    return Object.assign(e, {
                        value: e.initialValue !== undefined ? e.initialValue : this.getEmptyValue(e.type),
                    });
                });
                return state;
            });
        }
        return (
            <Modal backdrop="static" show={this.props.show} onHide={() => this.props.onClose(false, this.getData())}>
                <Modal.Header>
                    <Modal.Title>{this.props.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>{this.getBody(this.props.request)}</Modal.Body>
                <Modal.Footer>
                    <Button onClick={() => this.props.onClose(true, this.getData())} appearance="primary">
                        Ok
                    </Button>
                    <Button onClick={() => this.props.onClose(false, this.getData())} appearance="subtle">
                        Cancel
                    </Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

var data = {
    nodes: [
        {
            id: "0",
            label: "Nodedddddddddddddddddd",
            position: { x: 55, y: 55 },
            nodeType: "object",
        },
        {
            id: "1",
            label: "Node",
            position: { x: 55, y: 255 },
            nodeType: "relation",
        },
        {
            id: "2",
            label: "Node",
            position: { x: 155, y: 255 },
            nodeType: "object",
        },
    ],
    edges: [
        {
            source: "0",
            target: "1",
            key: "test",
        },
        {
            source: "0",
            target: "1",
        },
        {
            source: "0",
            target: "1",
        },
        {
            source: "0",
            target: "1",
        },
        {
            source: "1",
            target: "0",
        },
        {
            source: "1",
            target: "1",
        },
        {
            source: "1",
            target: "2",
        },
        {
            source: "2",
            target: "2",
        },
        {
            source: "2",
            target: "2",
        },
    ],
};

const vowels = "euioaуеыаоэяиюё";

class RObjectArea {
    constructor() {
        this.objects = [];
        this.relations = [];
    }

    getFreeId = () => {
        var id = 0;
        while (this.objects.concat(this.relations).filter((e) => e.id == `${id}`).length) {
            id += 1;
        }
        return `${id}`;
    };

    addObject = (
        id,
        name,
        params = {
            position: { x: 0, y: 0 },
            size: { width: 0, height: 0 },
            nodeType: "object",
        }
    ) => {
        this.objects.push({
            id: id || this.getFreeId(),
            label: name,
            params: params
                ? params
                : {
                      position: { x: 0, y: 0 },
                      size: { width: 0, height: 0 },
                      nodeType: "object",
                  },
        });
    };

    deleteObject = (id) => {
        this.objects = this.objects.filter((e) => e.id != id);
        this.relations = this.relations.filter((r) => r.source != id && r.target != id);
    };

    addRelation = (
        id,
        name,
        source,
        target,
        params = {
            position: { x: 0, y: 0 },
            nodeType: "relation",
            size: { width: 0, height: 0 },
        }
    ) => {
        this.relations.push({
            id: id || this.getFreeId(),
            source: source,
            target: target,
            label: name,
            params: params
                ? params
                : {
                      position: { x: 0, y: 0 },
                      nodeType: "relation",
                      size: { width: 0, height: 0 },
                  },
        });
    };

    deleteRelation = (id) => {
        this.relations = this.relations.filter((r) => r.id != id);
    };

    toData = () => {
        return {
            nodes: this.objects
                .map((e) => {
                    e.params.nodeType = "object";
                    return e;
                })
                .concat(
                    this.relations.map((e) => {
                        e.params.nodeType = "relation";
                        return e;
                    })
                )
                .map((e) => {
                    return {
                        id: e.id,
                        label: e.label,
                        position: e.params.position,
                        nodeType: e.params.nodeType,
                        position: e.params.position,
                        size: e.params.size,
                    };
                }),
            edges: this.relations
                .map((e) => Object({ id: e.id, source: e.source, target: e.id }))
                .concat(this.relations.map((e) => Object({ id: e.id, source: e.id, target: e.target }))),
        };
    };

    add = (e, edges) => {
        if (e.nodeType == "object") {
            this.addObject(e.id, e.label, { position: e.position, size: e.size });
        } else {
            this.addRelation(
                e.id,
                e.label,
                edges.filter((ed) => ed.target == e.id)[0].source,
                edges.filter((ed) => ed.source == e.id)[0].target,
                { position: e.position, size: e.size }
            );
        }
    };

    static getShort(word, count = 0) {
        word = word.replace(/\s/g, "");
        var short = word[0];
        var novow = word.slice(1);
        for (var v in vowels) {
            while (novow.indexOf(vowels[v]) != -1) {
                novow = novow.replace(vowels[v], "");
            }
        }
        if (count <= novow.length) {
            short += novow.slice(0, count);
        } else if (count <= word.length) {
            short += word.slice(1, count);
        } else {
            while (short.length + word.length != count + 1) {
                short += short;
            }
            short = short + word;
        }
        return short;
    }

    static getIES(words) {
        var prev = [];
        return words.map((w) => {
            var res = RObjectArea.getIE(w, prev);
            prev.push(res);
            return res;
        });
    }

    static getIE(word, prev = []) {
        var count = 0;
        while (prev.includes(RObjectArea.getShort(word, count))) {
            count += 1;
        }
        return RObjectArea.getShort(word, count);
    }

    static getPhraseIE(phrase, prev = []) {
        var ws = phrase.split(" ");
        var counts = ws.map((e) => 0);
        while (prev.includes(ws.map((w, i) => RObjectArea.getShort(w, counts[i])).join(""))) {
            counts[counts.indexOf(Math.min(...counts))] += 1;
        }
        return ws.map((w, i) => RObjectArea.getShort(w, counts[i])).join("");
    }

    getRelationsIES = () => {
        return this.relations.map((e) =>
            Object({
                id: e.id,
                ie: `{${this.getObjectIE(e.source)}, ${this.getObjectIE(e.target)}} ${e.label}`,
                nLabel: `${this.getObjectIE(e.source)} ${e.label} ${this.getObjectIE(e.target)}`,
            })
        );
    };

    getObjectIES = () => {
        var groups = this.objects
            .filter((e) => !this.objects.filter((o) => o.label == e.label).indexOf(e))
            .map((e) =>
                Object({
                    label: e.label,
                    ids: this.objects.filter((o) => o.label == e.label).map((o) => o.id),
                })
            );

        var ies = RObjectArea.getIES(groups.map((e) => e.label.toLowerCase()));
        return groups.map((e, i) =>
            Object(
                Object({
                    label: e.label,
                    ids: e.ids,
                    ie: ies[i],
                })
            )
        );
    };

    getObjectIE = (id) => {
        return (
            this.getObjectIES().filter((e) => e.ids.includes(id))[0].ie +
            (this.getObjectIES()
                .filter((e) => e.ids.includes(id))[0]
                .ids.indexOf(id) +
                1)
        );
    };

    get tableStyle() {
        return `<style type="text/css">
            table {
                border-collapse: collapse;
            }

            td, th {
                border: 1px solid black;
            }
        </style>`;
    }

    get objectsHTML() {
        return `<table>
            <tr>
                <th>Объекты</th>
            </tr>
            ${this.objects
                .filter((e) => !this.objects.filter((n) => n.label == e.label).indexOf(e))
                .map(
                    (e) => `<tr>
                <td>${e.label} ${
                        this.objects.filter((n) => n.label == e.label).length - 1
                            ? `(${this.objects.filter((n) => n.label == e.label).length} шт)`
                            : ""
                    }</td>
            </tr>`
                )
                .join("\n")}
        </table>`;
    }

    get relationsHTML() {
        return `<table>
            <tr>
                <th>Связи</th>
            </tr>
            ${this.relations
                .map(
                    (e) => `<tr>
                <td>${this.objects.filter((n) => n.id == e.source)[0].label} <span style="font-weight: bold;">${
                        e.label
                    }</span> ${this.objects.filter((n) => n.id == e.target)[0].label}</td>
            </tr>`
                )
                .join("\n")}
        </table>`;
    }

    get OIHTML() {
        return `<table>
            <tr>
                <th>Объекты</th><th>Идентификатор</th>
            </tr>
            ${this.objects
                .map(
                    (e) => `<tr>
                <td>${e.label}</td>
                <td>${this.getObjectIE(e.id)}
                </td>
            </tr>`
                )
                .join("\n")}
        </table>`;
    }

    get RIHTML() {
        return `<table>
            <tr>
                <th>Связи</th><th>Идентификатор</th>
            </tr>
            ${this.getRelationsIES()
                .map(
                    (e) => `<tr>
                <td>${e.nLabel}</td>
                <td>${e.ie}
                </td>
            </tr>`
                )
                .join("\n")}
        </table>`;
    }

    openReport = () => {
        var w = window.open();
        w.document.head.innerHTML = this.tableStyle;
        w.document.body.innerHTML = `<div>Этап 2. Выделение объектов и связей предметной области
        </div><div>
        <table>
            <tr>
                <td style="border: 0px">${this.objectsHTML}</td>
                <td style="border: 0px">${this.relationsHTML}</td>
            </tr>
        </table>
        </div>
        <div>Этап 3. Присваивание имен</div>
        <div>
        ${this.OIHTML}
        </div><br>
        <div>
        ${this.RIHTML}
        </div>
        `;
    };

    static fromData(data) {
        var res = new RObjectArea();
        data.nodes.map((e) => res.add(e, data.edges));
        return res;
    }
}

class App extends React.Component {
    constructor(props) {
        super(props);
        console.log(this, RObjectArea);
        this.state = {
            data: new RObjectArea().toData(),
            promptProps: { show: false, onClose: () => {}, request: [], title: "" },
        };
    }

    navClicked = (key) => {
        if (key == "0") {
            this.prompt(
                "Сохранение",
                [
                    { type: "label", text: "Введите имя файла" },
                    { type: "input", placeholder: "Имя файла", initialValue: "data" },
                ],
                (res, data) => {
                    if (res) {
                        if (data[1].value.replace(/\s/g, "") != "") {
                            download(`${data[1].value}.json`, JSON.stringify(this.getData()));
                        } else {
                            this.prompt("Ошибка", [{ type: "label", text: "Имя файла не должно быть пустым" }]);
                        }
                    }
                }
            );
        } else if (key == "1") {
            upload((json) => {
                this.setData(JSON.parse(json));
            });
        } else if (key == "2") {
            RObjectArea.fromData(this.getData()).openReport();
        } else if (key == "3" && typeof this.addNode == typeof (() => {})) {
            this.prompt(
                "Создание объекта",
                [
                    {
                        type: "label",
                        text: "Введите имя объекта",
                    },
                    {
                        type: "input",
                        placeholder: "Имя объекта",
                    },
                ],
                (result, data) => {
                    if (result) {
                        if (data[1].value.replace(/\s/g, "") != "") {
                            this.addNode({
                                id: this.getFreeNodeId(),
                                label: data[1].value,
                                nodeType: "object",
                            });
                        } else {
                            this.prompt("Ошибка", [
                                {
                                    type: "label",
                                    text: "Имя объекта не должно быть пустым",
                                },
                            ]);
                        }
                    }
                }
            );
        } else if (
            key == "4" &&
            typeof this.addNode == typeof (() => {}) &&
            typeof this.addEdge == typeof (() => {}) &&
            typeof this.getData == typeof (() => {})
        ) {
            this.prompt(
                "Создание связи",
                [
                    {
                        type: "label",
                        text: "Выберите начальный объект",
                    },
                    {
                        type: "select",
                        placeholder: "Объект",
                        data: this.getData()
                            .nodes.filter((e) => e.nodeType == "object")
                            .map((e) => Object({ value: e.id, label: e.label })),
                    },
                    {
                        type: "label",
                        text: "Выберите конечный объект",
                    },
                    {
                        type: "select",
                        placeholder: "Объект",
                        data: this.getData()
                            .nodes.filter((e) => e.nodeType == "object")
                            .map((e) => Object({ value: e.id, label: e.label })),
                    },
                    {
                        type: "label",
                        text: "Введите имя связи",
                    },
                    {
                        type: "input",
                        placeholder: "Имя связи",
                    },
                ],
                (result, data) => {
                    if (result) {
                        if (data[5].value.replace(/\s/g, "") != "" && data[1].value && data[3].value) {
                            var id = this.getFreeNodeId();
                            var nodes = this.getData().nodes;
                            this.addNode(
                                {
                                    id,
                                    label: data[5].value,
                                    nodeType: "relation",
                                    position: {
                                        x: parseInt(
                                            (nodes.filter((e) => e.id == data[1].value)[0].position.x +
                                                nodes.filter((e) => e.id == data[3].value)[0].position.x) /
                                                2
                                        ),
                                        y: parseInt(
                                            (nodes.filter((e) => e.id == data[1].value)[0].position.y +
                                                nodes.filter((e) => e.id == data[3].value)[0].position.y) /
                                                2
                                        ),
                                    },
                                },
                                () => {
                                    this.addEdge({ source: data[1].value, target: id }, () => {
                                        this.addEdge({ source: id, target: data[3].value });
                                    });
                                }
                            );
                        } else {
                            var req = [];
                            if (data[5].value.replace(/\s/g, "") == "") {
                                req.push({ type: "label", text: "Имя связи не должно быть пустым" });
                            }
                            if (!data[1].value) {
                                req.push({ type: "label", text: "Не выбран начальный объект" });
                            }
                            if (!data[3].value) {
                                req.push({ type: "label", text: "Не выбран конечный объект" });
                            }
                            this.prompt("Ошибка", req);
                        }
                    }
                }
            );
        }
    };

    gotData = (data) => {
        var d = data;
        setTimeout(
            () =>
                this.setState((state) => {
                    state.data = d ? d : state.currentData ? state.currentData : state.data;
                    state.open = false;
                    return state;
                }),
            1000
        );
    };

    onGraphInitialized = (commands) => {
        for (var command in commands) {
            this[command] = commands[command];
        }
    };

    prompt = (title, request, onClose) => {
        this.setState((state) => {
            state.promptProps = { show: true, onClose: this.closePrompt(onClose), request: request, title: title };
            return state;
        });
    };

    closePrompt = (callback) => (result, data) => {
        this.setState(
            (state) => {
                state.promptProps = Object.assign(state.promptProps, { show: false, onClose: () => {} });
                return state;
            },
            () => {
                if (typeof callback == typeof (() => {})) {
                    callback(result, data);
                }
            }
        );
    };

    edtNode = (nodeProps) => () => {
        if (typeof this.getData == typeof (() => {}) && typeof this.setData == typeof (() => {})) {
            document.body.click();
            var a = RObjectArea.fromData(this.getData());
            var req =
                nodeProps.initial.nodeType == "object"
                    ? [
                          { type: "label", text: "Задайте новое имя объекта" },
                          { type: "input", placeholder: "Имя объекта", initialValue: nodeProps.initial.label },
                      ]
                    : [
                          {
                              type: "label",
                              text: "Выберите новый начальный объект",
                          },
                          {
                              type: "select",
                              placeholder: "Объект",
                              data: this.getData()
                                  .nodes.filter((e) => e.nodeType == "object")
                                  .map((e) => Object({ value: e.id, label: e.label })),
                              initialValue: a.relations.filter((r) => r.id == nodeProps.id)[0].source,
                          },
                          {
                              type: "label",
                              text: "Выберите новый конечный объект",
                          },
                          {
                              type: "select",
                              placeholder: "Объект",
                              data: this.getData()
                                  .nodes.filter((e) => e.nodeType == "object")
                                  .map((e) => Object({ value: e.id, label: e.label })),
                              initialValue: a.relations.filter((r) => r.id == nodeProps.id)[0].target,
                          },
                          {
                              type: "label",
                              text: "Введите новое имя связи",
                          },
                          {
                              type: "input",
                              placeholder: "Имя связи",
                              initialValue: nodeProps.initial.label,
                          },
                      ];
            this.prompt("Редактирование", req, (res, data) => {
                if (res) {
                    if (nodeProps.initial.nodeType == "object") {
                        if (data[1].value.replace(/\s/g, "") != "") {
                            var d = this.getData();
                            d.nodes.filter((e) => e.id == nodeProps.id)[0].label = data[1].value;
                            this.setData(d);
                        } else {
                            this.prompt("Ошибка", [
                                {
                                    type: "label",
                                    text: "Имя объекта не должно быть пустым",
                                },
                            ]);
                        }
                    } else {
                        if (data[5].value.replace(/\s/g, "") != "" && data[1].value && data[3].value) {
                            var a = RObjectArea.fromData(this.getData());
                            a.deleteRelation(nodeProps.id);
                            a.addRelation(nodeProps.id, data[5].value, data[1].value, data[3].value, {
                                position: nodeProps.initial.position,
                                size: nodeProps.initial.size,
                                nodeType: "relation",
                            });
                            this.setData(a.toData());
                        } else {
                            var req = [];
                            if (data[5].value.replace(/\s/g, "") == "") {
                                req.push({ type: "label", text: "Имя связи не должно быть пустым" });
                            }
                            if (!data[1].value) {
                                req.push({ type: "label", text: "Не выбран начальный объект" });
                            }
                            if (!data[3].value) {
                                req.push({ type: "label", text: "Не выбран конечный объект" });
                            }
                            this.prompt("Ошибка", req);
                        }
                    }
                }
            });
        }
    };

    delNode = (nodeProps) => () => {
        if (typeof this.getData == typeof (() => {}) && typeof this.setData == typeof (() => {})) {
            document.body.click();
            this.prompt(
                "Подтверждение",
                [
                    {
                        type: "label",
                        text: `Удалить ${nodeProps.initial.nodeType == "object" ? "объект" : "связь"} "${
                            nodeProps.initial.label
                        }"?`,
                    },
                ],
                (res) => {
                    if (res) {
                        var a = RObjectArea.fromData(this.getData());
                        nodeProps.initial.nodeType == "object"
                            ? a.deleteObject(nodeProps.id)
                            : a.deleteRelation(nodeProps.id);
                        this.setData(a.toData());
                    }
                }
            );
        }
    };

    getNodePopover = (nodeProps) => {
        return (
            <Popover title="Опции">
                <List hover>
                    <List.Item style={{ cursor: "pointer" }} onClick={this.edtNode(nodeProps)}>
                        Изменить <Icon style={{ margin: 5 }} icon="pencil"></Icon>
                    </List.Item>
                    <List.Item style={{ cursor: "pointer" }} onClick={this.delNode(nodeProps)}>
                        Удалить<Icon style={{ margin: 5 }} icon="trash"></Icon>
                    </List.Item>
                </List>
            </Popover>
        );
    };

    getNodeContent = (nodeProps) => {
        return nodeProps.initial.nodeType == "object" ? (
            <div
                ref={nodeProps.ref}
                className={nodeProps.className}
                style={{
                    textAlign: "center",
                    border: "2px solid black",
                    borderRadius: 100,
                    boxShadow: "none",
                }}
            >
                <label>{nodeProps.initial.label}</label>
                <nodrag>
                    <Whisper trigger="click" placement="bottom" speaker={this.getNodePopover(nodeProps)}>
                        <span>
                            <Icon style={{ cursor: "pointer", margin: 3 }} icon="chevron-circle-down"></Icon>
                        </span>
                    </Whisper>
                </nodrag>
            </div>
        ) : (
            <div ref={nodeProps.ref} style={{ padding: 5 }} className={nodeProps.className}>
                <div style={{ textAlign: "center" }}>
                    <label>{nodeProps.initial.label}</label>
                    <nodrag>
                        <Whisper placement="bottom" trigger="click" speaker={this.getNodePopover(nodeProps)}>
                            <span>
                                <Icon style={{ cursor: "pointer", margin: 3 }} icon="chevron-circle-down"></Icon>
                            </span>
                        </Whisper>
                    </nodrag>
                </div>
                <div style={{ textAlign: "center" }}>
                    <div
                        style={{
                            display: "inline-block",
                            padding: 10,
                            border: "2px solid black",
                            borderRightWidth: 0,
                        }}
                    ></div>
                    <div style={{ display: "inline-block", padding: 10, border: "2px solid black" }}></div>
                </div>
            </div>
        );
    };

    render() {
        return (
            <Container style={{ height: "100%" }}>
                <Header>
                    <Navbar appearance="inverse">
                        <Navbar.Header>
                            <label
                                style={{
                                    padding: "18px 20px",
                                    display: "inline-block",
                                }}
                            >
                                МНИ
                            </label>
                        </Navbar.Header>
                        <Nav onSelect={this.navClicked}>
                            <Dropdown icon={<Icon icon="bars" />} title="Меню">
                                <Dropdown.Item eventKey="0">Скачать</Dropdown.Item>
                                <Dropdown.Item eventKey="1">Загрузить из файла</Dropdown.Item>
                                <Dropdown.Item eventKey="2">Отчет</Dropdown.Item>
                            </Dropdown>
                            <Nav.Item eventKey="3" icon={<Icon icon="plus" />}>
                                Добавить объект
                            </Nav.Item>
                            <Nav.Item eventKey="4" icon={<Icon icon="plus" />}>
                                Добавить связь
                            </Nav.Item>
                        </Nav>
                    </Navbar>
                </Header>
                <Container style={{ height: "100%", background: "#e6ecf5" }}>
                    <SuperGraph
                        onInitialized={this.onGraphInitialized}
                        width={"100%"}
                        height={"100%"}
                        data={this.state.data}
                        onEdgeContextMenu={(e, edge) => {
                            console.log(e, edge);
                            e.preventDefault();
                        }}
                        getNodeContent={this.getNodeContent}
                    ></SuperGraph>
                </Container>
                {this.state.promptProps.show ? <Prompt {...this.state.promptProps}></Prompt> : <></>}
            </Container>
        );
    }
}

export default App;
