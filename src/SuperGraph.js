import React from "react";
import Draggable from "react-draggable";

import "./SuperGraph.css";

class SuperNode extends React.Component {
    constructor(props) {
        super(props);
        this.DOMRef = React.createRef();
    }

    onDragStart = (e, ui) => {
        if (this.props.onDragStart && typeof this.props.onDragStart == typeof (() => {})) {
            this.props.onDragStart(
                {
                    id: this.props.id,
                    position: { x: ui.x, y: ui.y },
                    size: { width: this.DOMRef.current.offsetWidth, height: this.DOMRef.current.offsetHeight },
                },
                e,
                this
            );
        }
        return true;
    };

    onDrag = (e, ui) => {
        if (this.props.onDrag && typeof this.props.onDrag == typeof (() => {})) {
            this.params = {
                position: { x: ui.x, y: ui.y },
                size: { width: this.DOMRef.current.offsetWidth, height: this.DOMRef.current.offsetHeight },
            };
            this.props.onDrag(
                {
                    id: this.props.id,
                    position: { x: ui.x, y: ui.y },
                    size: { width: this.DOMRef.current.offsetWidth, height: this.DOMRef.current.offsetHeight },
                },
                e,
                this
            );
        }
        return true;
    };

    onDragEnd = (e, ui) => {
        if (this.props.onDragEnd && typeof this.props.onDragEnd == typeof (() => {})) {
            this.props.onDragEnd(
                {
                    id: this.props.id,
                    position: { x: ui.x, y: ui.y },
                    size: { width: this.DOMRef.current.offsetWidth, height: this.DOMRef.current.offsetHeight },
                },
                e,
                this
            );
        }
        return true;
    };

    componentDidMount = () => {
        if (typeof this.props.onRendered == typeof (() => {})) {
            this.props.onRendered(
                {
                    id: this.props.id,
                    position: this.props.position,
                    size: { width: this.DOMRef.current.offsetWidth, height: this.DOMRef.current.offsetHeight },
                },
                this
            );
        }
    };

    render() {
        var props = {};
        for (var p in this.props) {
            props[p] = this.props[p];
        }
        return (
            <Draggable
                bounds="parent"
                cancel="nodrag"
                defaultPosition={this.props.position}
                style={{ transform: `translate(${this.props.position.x}px, ${this.props.position.y}px)` }}
                onDrag={this.onDrag}
                onStart={this.onDragStart}
                onStop={this.onDragEnd}
            >
                {typeof this.props.getChildren == typeof (() => {}) ? (
                    this.props.getChildren(Object.assign(props, { ref: this.DOMRef, className: "super-graph-node" }))
                ) : (
                    <div className="super-graph-node">
                        <div>{this.props.content ? this.props.content : <>{this.props.initial.label}</>}</div>
                        {this.props.undragableContent ? <nodrag>{this.props.undragableContent}</nodrag> : <></>}
                    </div>
                )}
            </Draggable>
        );
    }
}

class SuperEdge extends React.Component {
    constructor(props) {
        super(props);
    }

    getPath = (source, target, order = 0, total = 1) => {
        var x = target.center.x - source.center.x;
        var y = target.center.y - source.center.y;
        var angle = source.id != target.id ? (Math.atan(y / x) * 180) / Math.PI : 0;

        var r1 = source.id != target.id ? total + Math.sqrt(x * x + y * y) / 20 : Math.sqrt(order + 1) * 25;
        var r2 = source.id != target.id ? order + (total % 2 ? order % 2 : 1 - (order % 2)) : Math.sqrt(order + 1) * 25;
        return `M${source.center.x},${source.center.y} A${r1},${r2} ${angle} ${
            source.id == target.id ? 1 : order % 2
        },${source.id == target.id ? 1 : order % 2} ${target.center.x - (source.id == target.id)},${
            target.center.y - (source.id == target.id)
        }`;
    };

    getArrow = (source, target, order = 0, total = 1) => {
        var [x1, y1] = [source.center.x, source.center.y];
        var x = target.center.x - source.center.x;
        var y = target.center.y - source.center.y;
        var angle = source.id != target.id ? (Math.atan(y / x) * 180) / Math.PI : 0;
        var R1 = source.id != target.id ? total + Math.sqrt(x * x + y * y) / 20 : Math.sqrt(order + 1) * 25;
        var R2 = source.id != target.id ? order + (total % 2 ? order % 2 : 1 - (order % 2)) : Math.sqrt(order + 1) * 25;

        var r1 = Math.sqrt(x * x + y * y);
        var r2 = (R2 * r1) / R1;

        var largeArcFlag = source.id == target.id ? 1 : order % 2;
        var sweepFlag = largeArcFlag;
        var [x2, y2] = [target.center.x - (source.id == target.id), target.center.y - (source.id == target.id)];
    };

    getHandler = (name) => (e) => {
        if (typeof this.props[name] == typeof (() => {})) {
            this.props[name](e, this.props.edge, this);
        }
    };

    getHandlers = () => {
        var res = {};
        ["onClick", "onDoubleClick", "onContextMenu"].map((e) => {
            res[e] = this.getHandler(e);
        });
        return res;
    };

    getStyle = () => Object({});

    renderPath = (edge, source, target, edges) => {
        var sameEdges = edges.filter((e) =>
            source.id != target.id
                ? [e.source, e.target].includes(source.id) && [e.source, e.target].includes(target.id)
                : e.source == e.target && e.source == source.id
        );

        [source, target] = sameEdges[0].source != source.id ? [target, source] : [source, target];

        source.center = {
            x: source.position.x + parseInt(source.size.width / 2),
            y: source.position.y + parseInt(source.size.height / 2),
        };

        target.center = {
            x: target.position.x + parseInt(target.size.width / 2),
            y: target.position.y + parseInt(target.size.height / 2),
        };
        var order = sameEdges.length - sameEdges.map((e) => e._SGEdgeAutoId).indexOf(edge._SGEdgeAutoId);
        var r = Math.sqrt(order) * 25;
        return (
            <path
                {...this.getHandlers()}
                style={this.getStyle()}
                d={this.getPath(source, target, order - 1, sameEdges.length)}
                className="super-graph-edge"
            ></path>
        );
    };

    render() {
        var source = this.props.allNodes.filter((n) => n.id == this.props.edge.source)[0];
        var target = this.props.allNodes.filter((n) => n.id == this.props.edge.target)[0];
        return this.renderPath(this.props.edge, source, target, this.props.allEdges);
    }
}

class SuperGraph extends React.Component {
    constructor(props) {
        super(props);
        console.log(0);

        var data = {
            nodes: props.data.nodes.map((e, i) => Object.assign(e, !e.size ? { size: { width: 0, height: 0 } } : {})),
            edges: props.data.edges.map((e, i) =>
                Object.assign(
                    e,
                    { _SGEdgeAutoId: `${i}` },
                    props.data.edges.filter((e) => !e.id).length ? { id: `${i}` } : {}
                )
            ),
        };

        this.state = {
            data: data,
            nodesRendered: false,
            edgeRendered: true,
            rerender: false,
        };
    }

    getFreeNodeId = () => {
        var id = 0;
        while (this.state.data.nodes.filter((e) => e.id == id).length) {
            id += 1;
        }
        return `${id}`;
    };

    addEdge = (edge, callback) => {
        this.setState(
            (state) => {
                if (state.data.edges.map((e) => e.id).indexOf(edge.id) == -1) {
                    state.data.edges.push(edge);
                    state.data.edges = state.data.edges.map((e, i) =>
                        Object.assign(
                            e,
                            { _SGEdgeAutoId: `${i}` },
                            state.data.edges.filter((e) => !e.id).length ? { id: `${i}` } : {}
                        )
                    );
                    state.rerender = true;
                    return state;
                }
            },
            () => {
                if (typeof callback == typeof (() => {})) {
                    callback(edge);
                }
            }
        );
    };

    addNode = (node, callback) => {
        this.setState(
            (state) => {
                if (state.data.nodes.map((e) => e.id).indexOf(node.id) == -1) {
                    state.data.nodes.push(node);
                    state.data.nodes = state.data.nodes.map((e, i) =>
                        Object.assign(
                            e,
                            !e.size ? { size: { width: 0, height: 0 } } : {},
                            !e.position ? { position: { x: 0, y: 0 } } : {}
                        )
                    );
                    state.rerender = true;
                    return state;
                }
            },
            () => {
                if (typeof callback == typeof (() => {})) {
                    callback(node);
                }
            }
        );
    };

    removeNode = (id, callback) => {
        this.setState(
            (state) => {
                state.data.edges = state.data.edges.filter((e) => e.target != id && e.source != id);
                state.data.nodes = state.data.nodes.filter((e) => e.id != id);
                state.rerender = true;
                return state;
            },
            () => {
                if (typeof callback == typeof (() => {})) {
                    callback(id);
                }
            }
        );
    };

    removeEdge = (id, callback) => {
        this.setState(
            (state) => {
                state.data.edges = state.data.edges.filter((e) => e.id != id);
                state.rerender = true;
                return state;
            },
            () => {
                if (typeof callback == typeof (() => {})) {
                    callback(id);
                }
            }
        );
    };

    getData = () => {
        return {
            nodes: this.state.data.nodes.map((e) => e),
            edges: this.state.data.edges.map((e) => e),
        };
    };

    setData = (data, callback) => {
        var done = false;
        this.setState(
            (state) => {
                if (!done) {
                    done = true;
                    state.data.nodes = data.nodes.map((e, i) =>
                        Object.assign(e, !e.size ? { size: { width: 0, height: 0 } } : {})
                    );
                    state.data.edges = data.edges.map((e, i) =>
                        Object.assign(
                            e,
                            { _SGEdgeAutoId: `${i}` },
                            state.data.edges.filter((e) => !e.id).length ? { id: `${i}` } : {}
                        )
                    );
                    state.rerender = true;
                    return state;
                }
            },
            () => {
                if (typeof callback == typeof (() => {})) {
                    callback(data);
                }
            }
        );
    };

    onNodeDrag = (p, e, n) => {
        this.setState((state) => {
            var node = this.state.data.nodes.filter((n) => n.id == p.id)[0];
            node.position = p.position;
            node.size = p.size;
            return state;
        });
    };

    onNodeRendered = (p, n) => {
        this.setState((state) => {
            var node = this.state.data.nodes.filter((n) => n.id == p.id)[0];
            node.position = p.position;
            node.size = p.size;
            return state;
        });
    };

    getNodeHandlers = () => {
        var res = {};
        for (var p in this.props) {
            if (p.indexOf("onNode") == 0) {
                res[p.replace("onNode", "on")] = this.props[p];
            }
        }
        return res;
    };

    getEdgeHandlers = () => {
        var res = {};
        for (var p in this.props) {
            if (p.indexOf("onEdge") == 0) {
                res[p.replace("onEdge", "on")] = this.props[p];
            }
        }
        return res;
    };

    renderNodes = () => {
        this.nodes = [];
        return this.state.data.nodes.map((node) => {
            var nodeProps = Object.assign(
                {
                    onRendered: this.onNodeRendered,
                    onDrag: this.onNodeDrag,
                    id: node.id,
                    initial: node,
                    position: node.position,
                },
                this.getNodeHandlers()
            );
            if (typeof this.props.getNodeContent == typeof (() => {})) {
                nodeProps.getChildren = this.props.getNodeContent;
            }
            return <SuperNode {...nodeProps}></SuperNode>;
        });
    };

    ready = () => {
        if (typeof this.props.onInitialized == typeof (() => {})) {
            this.props.onInitialized({
                getFreeNodeId: this.getFreeNodeId,
                addNode: this.addNode,
                addEdge: this.addEdge,
                removeNode: this.removeNode,
                removeEdge: this.removeEdge,
                getData: this.getData,
                setData: this.setData,
            });
        }
    };

    componentDidMount = () => {
        if (!this.state.nodesRendered) {
            this.setState(
                (state) => {
                    state.nodesRendered = true;
                    return state;
                },
                () => {
                    this.ready();
                }
            );
        }
    };

    renderEdges = () => {
        return this.state.data.edges.map((edge) => {
            var edgeProps = Object.assign(
                {
                    edge: edge,
                    allEdges: this.state.data.edges,
                    allNodes: this.state.data.nodes,
                    orient: this.props.orient,
                },
                this.getEdgeHandlers()
            );
            if (typeof this.props.getEdgeContent == typeof (() => {})) {
                this.props.getEdgeContent(edge, edgeProps);
            } else {
                return <SuperEdge {...edgeProps}></SuperEdge>;
            }
        });
    };

    render() {
        if (this.state.rerender) {
            this.setState((state) => {
                state.rerender = false;
                return state;
            });
            return <div style={{ width: this.props.width, height: this.props.height }} class="super-graph-root"></div>;
        }
        return (
            <div style={{ width: this.props.width, height: this.props.height }} class="super-graph-root">
                <div class="super-graph-nodes">{this.renderNodes()}</div>
                <svg className="super-graph-edges">{this.state.nodesRendered ? this.renderEdges() : <></>}</svg>
            </div>
        );
    }
}

export default SuperGraph;
